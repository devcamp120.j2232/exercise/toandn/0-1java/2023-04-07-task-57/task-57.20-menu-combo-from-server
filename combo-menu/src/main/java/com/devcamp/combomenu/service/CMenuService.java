package com.devcamp.combomenu.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.combomenu.models.CMenu;

@Service
public class CMenuService {
  CMenu small = new CMenu("S (Small size)", "20 cm", 2, "200 gr", 2, "150.000");
  CMenu medium = new CMenu("M (Medium size)", "25 cm", 4, "300 gr", 3, "200.000");
  CMenu large = new CMenu("L (Large size)", "30 cm", 8, "500 gr", 4, "250.000");

  ArrayList<CMenu> listMenu = new ArrayList<>();

  public ArrayList<CMenu> getComboMenu() {
    ArrayList<CMenu> allMenu = new ArrayList<>();
    allMenu.add(small);
    allMenu.add(medium);
    allMenu.add(large);

    listMenu.addAll(allMenu);

    return allMenu;
  }

}
