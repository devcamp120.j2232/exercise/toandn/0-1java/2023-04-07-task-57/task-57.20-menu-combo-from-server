package com.devcamp.combomenu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComboMenuApplication {

	public static void main(String[] args) {
		SpringApplication.run(ComboMenuApplication.class, args);
	}

}
